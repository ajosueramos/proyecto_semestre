from django.shortcuts import render
from django.views import View

class InicioView(View):
    def get(self, request,*args,**kwargs):
        return render(request, 'base.html', {'titulo':'Menú Principal','url_anterior':'/'})

# Create your views here.
