from django.db import models

# Create your models here.
class Linea(models.Model):
    descripcion = models.CharField("Linea",max_length=100,unique=True)
    imagen = models.FileField(verbose_name="Foto", upload_to="core/Linea", blank=True, null=True)
    estado = models.BooleanField(default=True)

    class Meta:
        verbose_name="Linea Producto"
        verbose_name_plural="Lineas Productos"
        ordering=("id",)
    def __str__(self):
        return "{}".format(self.descripcion)

class Grupo(models.Model):
    linea = models.ForeignKey(Linea, on_delete=models.PROTECT, blank=True, null=True)
    descripcion = models.CharField("Grupo",max_length=100,unique=True)
    imagen = models.FileField(verbose_name="Foto", upload_to="core/Grupo", blank=True, null=True)
    estado = models.BooleanField(default=True)

    class Meta:
        verbose_name="Categoria"
        verbose_name_plural="Categorias"
        ordering=("id",)

    def __str__(self):
        return "{}".format(self.descripcion)